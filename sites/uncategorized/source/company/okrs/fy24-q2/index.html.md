---
layout: markdown_page
title: "FY24-Q2 OKRs"
description: "View GitLabs Objective-Key Results for FY24 Q2. Learn more here!"
canonical_path: "/company/okrs/fy24-q2/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from 2023-05-01 to 2023-07-31.

The source of truth for FY24-Q2 OKRs will be in GitLab.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2023-03-27 | CEO shares top goals with E-group for feedback |
| -4 | 2023-04-03 | CEO shares top goals in #okrs Slack channel |
| -4 | 2023-04-03 | E-group propose OKRs for their functions in the OKR draft review meeting agenda |
| -3 | 2023-04-10 | E-group 50 minute draft review meeting | 
| -2 | 2023-04-17 | E-group discusses with their respective teams and polishes OKRs |
| -2 | 2023-04-17 | CEO top goals available in GitLab | 
| -1 | 2023-04-24 | Function OKRs are put into GitLab and links are shared in #okrs Slack channel |
| -1 | 2023-04-24 | CEO reports post links to final OKRs in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2023-05-01 | CoS to the CEO updates OKR page for current quarter to be active and includes CEO level OKRs with consideration to what is public and non-public |


## OKRs

The source of truth for GitLab OKRs and KRs is [GitLab](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?label_name%5B%5D=CEO%20OKR&milestone_title=FY24-Q2). CEO objectives and KRs are captured on this page. 

### 1. CEO: [Continue to win against GitHub](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1875)
1. **CEO KR**: [Develop comprehensive GitHub compete narrative with new content positioning stronger AI leadership, sales/rev play campaign](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1962)
1. **CEO KR**: [Achieve 100% of the CRO org having completed the GitHub competitive messaging training](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1963)
1. **CEO KR**: [Define R&D priorities → Share updated roadmap that reflects near-term win requirements](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1964)

### 2. CEO: [Have AI in all we do](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1882)
1. **CEO KR**: [Quickly deliver AI Assisted features including 48 experimental features, 16 beta features, and 8 GA features on GitLab.com](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1966)
1. **CEO KR**: [Achieve 100% of the CRO org having completed the AI competitive messaging training](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1967)
1. **CEO KR**: [David or Sid conduct an exclusive, in-depth AI briefing, including a product roadmap, with a tier-one AMER media outlet, to lay the foundation for our AI strategy](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1968)
1. **CEO KR**: [Continue briefing cadence with 5-7 key publications across EMEA and APJ once the exclusive runs in AMER](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1969)

### 3. CEO: [Deliver predictable high value to customers by reducing churn and contraction](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1893)
1. **CEO KR**: [Reduce churn and contraction by X% compared to ATR FY23-Q4](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1970)
1. **CEO KR**: [Overachieve on Professional Services attach rate at X% of Q2 Net ARR (target in plan = Y%) via mandatory Ultimate attach initiative, to drive improved product adoption](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1971)
1. **CEO KR**: [>90% of top 50 customers by ARR pitched about GitLab’s AI strategy in H1 FY24, with unbranded content in hand for them to use internally in their own AI advocacy](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1972)
1. **CEO KR**: [Support 3 customers with >$100K ARR to contribute 10+ MRs in the quarter](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1973)
1. **CEO KR**: [Deliver 5 customer value stories (1 marquee video, big names preferred) with clear ROI outlined (shared with sales)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1974)

### 4. CEO: [Make GitLab easier to do business with](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1899)
1. **CEO KR**: Complete 4 Q2 Fulfillment initiatives:  [scope temporary licenses technical solution](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/6124), [complete web store discount work](https://gitlab.com/groups/gitlab-org/-/epics/10245), [single customer definition across Q2C systems](https://gitlab.com/groups/gitlab-org/-/epics/8951), and [deliver first iteration of project Juvet](https://gitlab.com/groups/gitlab-org/-/epics/10398): https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1975  
1. **CEO KR**: [Collaborate with hyperclouds to investigate offering GitLab as a native 1Platform service in the Cloud provider's console](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1978)
1. **CEO KR**: [Have Dedicated in GA, onboard target, and reduce minimum seat count to 1k Ultimate seats](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1979)


### 5. CEO: [Continue to build a diverse team of top talent that we retain and grow](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1905)
1. **CEO KR**: [Reduce regrettable turnover by 0.3%](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1980)
1. **CEO KR**: [Increase our URG population from 17% to 18% and increase female population from 31% to 33%](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1981)
1. **CEO KR**: [Increase leadership Elevate enrollees from 10% to 30% of our people managers with an annualized target of 85%](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1982)
